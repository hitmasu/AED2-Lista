/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aedii.controle;

import aedii.model.Usuario;

/**
 *
 * @author Zack
 */
public class ListaEncadeada {

    private int quantidade;
    private No inicio;

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    
    public No getInicio(){
        return inicio;
    }
    
    public ListaEncadeada(){
        setQuantidade(0);
        inicio = null;
    }
    
    public boolean inserirDadoFim(Usuario usuario){
        return inserirDado(usuario, getQuantidade());
    }

    public boolean inserirDado(Usuario usuario, int posicao) {
        if (posicao < 0 || posicao > getQuantidade()) {
            return false;
        } else {
            No noAnterior = getNoAnterior(posicao);

            if (noAnterior == null) {
                setInicio(usuario);
            } else {
                No novoNo = new No(usuario, noAnterior.getProximo());
                noAnterior.setProximo(novoNo);
            }
            setQuantidade(getQuantidade()+1);
            return true;
        }
    }

    private No getNoAnterior(int posicao) {
        No noProximo = inicio;
        
        int count = 0;

        while (count < posicao-1) {
            noProximo = noProximo.getProximo();
            count++;
        }
        
        return noProximo;
    }
    
    public No pesquisarUsuario(int codigo){
        No noProximo = inicio;
        Usuario usuario = noProximo.getUsuario();
        
        while(usuario.getCodigo() != codigo){
            noProximo = noProximo.getProximo();
            if(noProximo == null){
                break;
            }
            usuario = noProximo.getUsuario();
            
        }
        
        return noProximo;
    }

    public boolean removerDado(int posicao) {
        if (posicao < 0 || posicao > getQuantidade()-1) {
            return false;
        } else {
            No noAnterior = getNoAnterior(posicao);
            if (posicao == getQuantidade() - 1) {
                noAnterior = null;
            } else {
                No noPosicao = noAnterior.getProximo();
                noAnterior.setProximo(noPosicao.getProximo());
            }
            setQuantidade(getQuantidade()-1);
            return true;
        }
    }

    public void setInicio(Usuario usuario) {
        No noInicio = new No(usuario);

        if (inicio != null) {
            noInicio.setProximo(inicio);
        }

        inicio = noInicio;
    }
    
    public boolean atualizarDado(Usuario usuario, int posicao){
        if (posicao < 0 || posicao > getQuantidade()-1) {
            return false;
        } else {
            No noPosicao = getNoAnterior(posicao+1);
            noPosicao.setUsuario(usuario);
            return true;
        }
    }
}
