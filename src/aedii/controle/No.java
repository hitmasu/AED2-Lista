/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aedii.controle;

import aedii.model.Usuario;

/**
 *
 * @author Zack
 */
public class No {
    private Usuario usuario;
    private No proximo;
    
    public No(Usuario usuario){
        this.usuario = usuario;
    }
    
    public No(Usuario usuario, No proximo){
        this.usuario = usuario;
        this.proximo = proximo;
    }
    
    public No getProximo(){
        return this.proximo;
    }
    
    public void setProximo(No proximo){
        this.proximo = proximo;
    }
    
    public Usuario getUsuario(){
        return this.usuario;
    }
    
    public void setUsuario(Usuario usuario){
        this.usuario = usuario;
    }
}
