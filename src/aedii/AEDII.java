/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aedii;

import aedii.controle.ListaEncadeada;
import aedii.controle.No;
import aedii.model.Usuario;

/**
 *
 * @author Zack
 */
public class AEDII {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Usuario usuario = new Usuario(0,"Usuario 1");
        Usuario usuario1 = new Usuario(1,"Usuario 2");
        Usuario usuario2 = new Usuario(2,"Usuario 3");
        Usuario usuario3 = new Usuario(3,"Usuario 4");
        Usuario usuario4 = new Usuario(4,"Usuario 5");
        Usuario usuario5 = new Usuario(5,"Usuario 6");
        
        ListaEncadeada lista = new ListaEncadeada();
        lista.inserirDadoFim(usuario);
        lista.inserirDadoFim(usuario1);
        lista.inserirDadoFim(usuario2);
        lista.inserirDadoFim(usuario3);
        lista.inserirDadoFim(usuario4);
        
        System.out.println("--Cadastro--");
        
        No noTeste = lista.getInicio();
        for (int i = 0; i < lista.getQuantidade(); i++) {
            System.out.println(noTeste.getUsuario().getNome());
            noTeste = noTeste.getProximo();
        }
                   
        System.out.println("--Remoção--");
        lista.removerDado(2);
        
        noTeste = lista.getInicio();
        for (int i = 0; i < lista.getQuantidade(); i++) {
            System.out.println(noTeste.getUsuario().getNome());
            noTeste = noTeste.getProximo();
        }
        
        System.out.println("--Pesquisa--");
        
        noTeste = lista.pesquisarUsuario(4);
        System.out.println(noTeste.getUsuario().getNome());
        
        System.out.println("--Atualiza--");
        
        System.out.println("**Lista Atual**");
        noTeste = lista.getInicio();
        for (int i = 0; i < lista.getQuantidade(); i++) {
            System.out.println(noTeste.getUsuario().getNome());
            noTeste = noTeste.getProximo();
        }
        
        Usuario usuarioNovo = new Usuario(100, "Usuario Teste");
        lista.atualizarDado(usuarioNovo, 2);
        
        System.out.println("**Lista Atualizada**");
        noTeste = lista.getInicio();
        for (int i = 0; i < lista.getQuantidade(); i++) {
            System.out.println(noTeste.getUsuario().getNome());
            noTeste = noTeste.getProximo();
        }
        
    }
    
}
